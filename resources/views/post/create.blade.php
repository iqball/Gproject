@extends('layouts.app')
@section('judul')
buat
@endsection
@section('content')
  <div class="container">
    <form class="" action="{{ route('post.store') }}" method="post">
      {{ csrf_field() }}
      <div class="form-group">
        <label for="">Title</label>
        <input type="text" class="form-control" name="title" placeholder="Masukan judul..">
      </div>
      <div class="form-group">
        <label for="">Category</label>
        <select class="form-control" name="category_id">
          @foreach($categories as $category)
            <option value="{{$category->id}}">{{$category->name}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="">Content</label>
        <textarea name="content" rows="5" class="form-control" placeholder="Masukan content.."></textarea>
      </div>
      <div class="form-group">
        <input type="submit" name="" class="btn btn-primary" value="SIMPAN">
      </div>
    </form>
  </div>
@endsection
